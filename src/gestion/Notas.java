/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestion;
import java.util.*;
/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
class Notas {
    static int[][] inicializarNotas(int numeroAlumnos) {
        int ListaNotas [][]=new int [6][numeroAlumnos];
        return ListaNotas;
    }

    static int[][] aniadirNota(int[][] listaNotas) {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Introduce el número del alumno");
        int nAlumno = teclado.nextInt();
        nAlumno=nAlumno-1;
        System.out.println("¿introduce el número de la asignatura que deseas guardar la nota?");
        System.out.println("    1-Programación");
        System.out.println("    2-Bases de datos");
        System.out.println("    3-Sistemas Informáticos");
        System.out.println("    4-Lenguajes de Marcas");
        System.out.println("    5-Formación y Orientación Laboral");
        System.out.println("    6-Entornos de Desarrollo");        
        int Numeroasignatura = teclado.nextInt();
        switch(Numeroasignatura){
            case(1):
                System.out.print("Introduce la nota de programación: ");
                listaNotas[Numeroasignatura-1][nAlumno]=teclado.nextInt();
            break;
            case(2):
                System.out.print("Introduce la nota de Bases de datos: ");
                listaNotas[Numeroasignatura-1][nAlumno]=teclado.nextInt();
            break;
            case(3):
                System.out.print("Introduce la nota de Sistemas Informáticos: ");
                listaNotas[Numeroasignatura-1][nAlumno]=teclado.nextInt();
            break;
            case(4):
                System.out.print("Introduce la nota de Lenguajes de Marcas: ");
                listaNotas[Numeroasignatura-1][nAlumno]=teclado.nextInt();
            break;
            case(5):
                System.out.print("Introduce la nota de Formación y Orientación Laboral: ");
                listaNotas[Numeroasignatura-1][nAlumno]=teclado.nextInt();
            break;
            case(6):
                System.out.print("Introduce la nota de Entornos de desarrollo: ");
                listaNotas[Numeroasignatura-1][nAlumno]=teclado.nextInt();
            break; 
        }
        return listaNotas;
    }

    static void mostrarNotasAlumno(int[][] listaNotas) {
        Scanner teclado = new Scanner(System.in);
        int Nalumno;
        System.out.println("introduce el número del alumno");
        Nalumno=teclado.nextInt();
            System.out.println("la nota del alumno en la asigantura de programación es : "+listaNotas[0][Nalumno-1]);
            System.out.println("la nota del alumno en la asigantura de Bases de datos es : "+listaNotas[1][Nalumno-1]);
            System.out.println("la nota del alumno en la asigantura de Sistemas Informáticos es : "+listaNotas[2][Nalumno-1]);
            System.out.println("la nota del alumno en la asigantura de Lenguaje de Marcas es : "+listaNotas[3][Nalumno-1]);
            System.out.println("la nota del alumno en la asigantura de Formación y Orientación Laboral es : "+listaNotas[4][Nalumno-1]);
            System.out.println("la nota del alumno en la asigantura de Entornos de Desarrollo es : "+listaNotas[5][Nalumno-1]);
            
    }
}
